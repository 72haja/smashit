//
//  GameField.swift
//  SmashIT
//
//  Created by student on 23.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class GameField: SKSpriteNode {
    
    func getOverlays() -> [SKSpriteNode] {
        return []
    }
    
    func getGoal() -> [Goal] {
        return []
    }
    
    func getInitialPositionsTeam1() -> [CGPoint] {
        return []
    }
    
    func getInitialPositionsTeam2() -> [CGPoint] {
        return []
    }
    
    func setPhysicBodyForPlayer(players: [Player]){
    }
    
    func setPhysicBodyForPlayer(players: [Player], radius: CGFloat){
    }
    
    func setPhysicsBodyForBall(ball: Ball) {
    }
}
