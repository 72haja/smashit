//
//  Goal.swift
//  SmashIT
//
//  Created by student on 23.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class Goal: SKSpriteNode {
    
    let goalName: String
    let goalTexture: SKTexture
    let goalLineTexture: SKTexture
    var goalLine: SKSpriteNode
    
    init(position: CGPoint, goalImage: String, goalLine: String, goalName: String) {
        self.goalName = goalName
        self.goalTexture = SKTexture(imageNamed: goalImage)
        self.goalLineTexture = SKTexture(imageNamed: goalLine)
        self.goalLine = SKSpriteNode(texture: goalLineTexture)
        super.init(texture: goalTexture, color: UIColor.clear, size: CalcSize.calcRelativeNodeSize(size: CGSize(width: goalTexture.size().width, height: goalTexture.size().height)))
        self.position = position
        setUpGoal()
        createGoalLine()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpGoal() {
        zPosition = 0.5
        physicsBody = SKPhysicsBody(texture: goalTexture, size: self.size)
        physicsBody?.pinned = true
        physicsBody?.isDynamic = false
        physicsBody?.categoryBitMask = GameScene.wallCategory
        physicsBody?.collisionBitMask = GameScene.noCategory
        physicsBody?.contactTestBitMask = GameScene.noCategory
    }
    
    func getPos() -> CGFloat {
        return (position.x + size.width/2 - 15)
    }
    
    func createGoalLine() {
        goalLine = SKSpriteNode(texture: goalLineTexture)
        goalLine.name = goalName
        self.goalLine.size = CalcSize.calcRelativeNodeSize(size: CGSize(width: goalLineTexture.size().width, height: goalLineTexture.size().height))
        goalLine.physicsBody = SKPhysicsBody(texture: goalLineTexture, size: goalLine.size)
        goalLine.physicsBody?.categoryBitMask = GameScene.goalCategory
        goalLine.physicsBody?.collisionBitMask = GameScene.noCategory
        goalLine.physicsBody?.contactTestBitMask = GameScene.ballCategory
        goalLine.physicsBody?.isDynamic = false
        goalLine.zPosition = 0
        self.addChild(goalLine)
    }
}
