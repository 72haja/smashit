//
//  Field.swift
//  SmashIT
//
//  Created by student on 17.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class IceHockeyField: GameField {
    
    init(frame: CGRect) {
        let texture = SKTexture(imageNamed: "icehockey_field")
        super.init(texture: texture, color: UIColor.clear, size: frame.size)
        size = frame.size
        position = CGPoint(x: 0, y: 0)
        zPosition = 0
        physicsBody = SKPhysicsBody(edgeLoopFrom: frame)
        physicsBody?.categoryBitMask = GameScene.wallCategory
        physicsBody?.collisionBitMask = GameScene.noCategory
        physicsBody?.pinned = true
    }
    
    override func getGoal() -> [Goal] {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width / 2
        
        let goallinks = IceHockeyGoal(position: CGPoint(x: 0, y: 0), goalImage: "icehockey_goal_left", goalLine: "icehockey_goal_line_left", goalName: "leftGoal")
        goallinks.position.x = screenWidth * -0.648 - goallinks.size.width/2
        let goalrechts = IceHockeyGoal(position: CGPoint(x: 0, y: 0), goalImage: "icehockey_goal_right", goalLine: "icehockey_goal_line_right", goalName: "rightGoal")
        goalrechts.position.x = screenWidth * 0.648 + goalrechts.size.width/2
        return [goalrechts, goallinks]
    }
    
    override func getOverlays() -> [SKSpriteNode] {
        let outlinesImages = ["icehockey_corner_top_left", "icehockey_corner_top_right", "icehockey_corner_bottom_right", "icehockey_corner_bottom_left"]
        let imagePosition = [[-1.0, 1.0], [1.0, 1.0], [1.0, -1.0], [-1.0, -1.0]]
        let frameHalfX = Double(frame.size.width / 2)
        let frameHalfY = Double(frame.size.height / 2)
        
        var overlays: [SKSpriteNode] = []
        for i in 0...3 {
            let fieldOverlay = SKTexture(imageNamed: outlinesImages[i])
            let fieldOverlayNode = SKSpriteNode(texture: fieldOverlay)
            let halfX = Double(fieldOverlayNode.frame.size.width / 2)
            let halfY = Double(fieldOverlayNode.frame.size.height / 2)
            let x = frameHalfX * imagePosition[i][0] - halfX * imagePosition[i][0]
            let y = frameHalfY * imagePosition[i][1] - halfY * imagePosition[i][1]
            fieldOverlayNode.position = CGPoint(x: x, y: y)
            fieldOverlayNode.physicsBody = SKPhysicsBody(texture: fieldOverlay, size: fieldOverlay.size())
            fieldOverlayNode.physicsBody?.pinned = true
            fieldOverlayNode.physicsBody?.isDynamic = false
            fieldOverlayNode.physicsBody?.categoryBitMask = GameScene.wallCategory
            fieldOverlayNode.physicsBody?.collisionBitMask = GameScene.noCategory
            fieldOverlayNode.physicsBody?.contactTestBitMask = GameScene.noCategory

            overlays.append(fieldOverlayNode)
        }
        return overlays
    }
    
    override func getInitialPositionsTeam1() -> [CGPoint] {
        return [CalcSize.calcNodePosition(position: CGPoint(x: 0.62, y: 0.44)),
                CalcSize.calcNodePosition(position: CGPoint(x: 0.54, y: 0.23)),
                CalcSize.calcNodePosition(position: CGPoint(x: 0.430, y: 0)),
                CalcSize.calcNodePosition(position: CGPoint(x: 0.54, y: -0.23)),
                CalcSize.calcNodePosition(position: CGPoint(x: 0.62, y: -0.44)),]
    }
    
    override func getInitialPositionsTeam2() -> [CGPoint] {
        return [CalcSize.calcNodePosition(position: CGPoint(x: -0.62, y: 0.44)),
                CalcSize.calcNodePosition(position: CGPoint(x: -0.54, y: 0.23)),
                CalcSize.calcNodePosition(position: CGPoint(x: -0.430, y: 0)),
                CalcSize.calcNodePosition(position: CGPoint(x: -0.54, y: -0.23)),
                CalcSize.calcNodePosition(position: CGPoint(x: -0.62, y: -0.44)),]
    }
    
    override func setPhysicBodyForPlayer(players: [Player]){
        let radius = CGFloat(CalcSize.calcRelativeNodeWidth(width: 30))
        setPhysicBodyForPlayer(players: players, radius: radius)
    }
    
    override func setPhysicBodyForPlayer(players: [Player], radius: CGFloat) {
        for p in players {
            p.physicsBody = SKPhysicsBody(circleOfRadius: radius)
            p.size = CGSize(width: radius*2, height: radius*2)
            p.physicsBody?.restitution = 1.0  // bouncyness
            p.physicsBody?.mass = 0.5         // trägheit
            p.physicsBody?.friction = 0.1       // well.. friction
            p.physicsBody?.linearDamping = 0.1  // slow down when moving
            p.physicsBody?.angularDamping = 1 // slow down when spinning
            p.physicsBody?.allowsRotation = false
            p.physicsBody?.categoryBitMask = GameScene.playerCategory
            p.physicsBody?.collisionBitMask = GameScene.playerCategory | GameScene.ballCategory | GameScene.wallCategory
            p.physicsBody?.contactTestBitMask = GameScene.powerUpCategory
        }
    }
    
    override func setPhysicsBodyForBall(ball: Ball) {
        ball.physicsBody?.restitution = 1.0   // bouncyness
        ball.physicsBody?.mass = 0.5          // trägheit
        ball.physicsBody?.friction = 0.1      // friction
        ball.physicsBody?.linearDamping = 0.3   // slow down when moving
        ball.physicsBody?.angularDamping = 0.5  // slow down when spinning
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

