//
//  Field.swift
//  SmashIT
//
//  Created by student on 17.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class SoccerField: GameField {
    
    init(frame: CGRect) {
        let texture = SKTexture(imageNamed: "soccer_field")
        super.init(texture: texture, color: UIColor.clear, size: frame.size)
        size = frame.size
        position = CGPoint(x: 0, y: 0)
        zPosition = 0
        physicsBody = SKPhysicsBody(edgeLoopFrom: frame)
        physicsBody?.categoryBitMask = GameScene.wallCategory
        physicsBody?.collisionBitMask = GameScene.noCategory
        physicsBody?.pinned = true
    }
    
    override func getGoal() -> [Goal] {
        let goallinks = SoccerGoal(position: CGPoint(x: 0, y: 0), goalImage: "soccer_goal_left", goalLine: "soccer_goal_line_left", goalNet: "soccer_goal_net_left", goalName: "leftGoal")
        goallinks.position = CGPoint(x: -frame.size.width / 2 + goallinks.size.width / 3, y: 0)
        let goalrechts = SoccerGoal(position: CGPoint(x: 0, y: 0), goalImage: "soccer_goal_right", goalLine: "soccer_goal_line_right", goalNet: "soccer_goal_net_right", goalName: "rightGoal")
        goalrechts.position = CGPoint(x: frame.size.width / 2 - goalrechts.size.width / 3, y: 0)
        return [goalrechts, goallinks]
    }
    
    override func getInitialPositionsTeam1() -> [CGPoint] {
        return [CalcSize.calcNodePosition(position: CGPoint(x: 0.63, y: 0.2)), CalcSize.calcNodePosition(position: CGPoint(x: 0.63, y: -0.2)) , CalcSize.calcNodePosition(position: CGPoint(x: 0.35, y: 0))]
    }
    
    override func getInitialPositionsTeam2() -> [CGPoint] {
        return [CalcSize.calcNodePosition(position: CGPoint(x: -0.63, y: 0.2)), CalcSize.calcNodePosition(position: CGPoint(x: -0.63, y: -0.2)) , CalcSize.calcNodePosition(position: CGPoint(x: -0.35, y: 0))]
    }
    
    override func setPhysicBodyForPlayer(players: [Player]){
        let radius = CGFloat(CalcSize.calcRelativeNodeWidth(width: 50))
        setPhysicBodyForPlayer(players: players, radius: radius)
    }
    
    override func setPhysicBodyForPlayer(players: [Player], radius: CGFloat) {
        for p in players {
            p.physicsBody = SKPhysicsBody(circleOfRadius: radius)
            p.size = CGSize(width: radius*2, height: radius*2)
            p.physicsBody?.restitution = 1.0  // bouncyness
            p.physicsBody?.restitution = 1.0  // bouncyness
            p.physicsBody?.mass = 0.7         // trägheit
            p.physicsBody?.friction = 0.2       // well.. friction
            p.physicsBody?.linearDamping = 0.4  // slow down when moving
            p.physicsBody?.angularDamping = 1 // slow down when spinning
            p.physicsBody?.allowsRotation = false
            p.physicsBody?.categoryBitMask = GameScene.playerCategory
            p.physicsBody?.collisionBitMask = GameScene.playerCategory | GameScene.ballCategory | GameScene.wallCategory
            p.physicsBody?.contactTestBitMask = GameScene.powerUpCategory
        }
    }
    
    override func setPhysicsBodyForBall(ball: Ball) {
        ball.physicsBody?.restitution = 1.0   // bouncyness
        ball.physicsBody?.mass = 0.5          // trägheit
        ball.physicsBody?.friction = 0.2      // friction
        ball.physicsBody?.linearDamping = 1   // slow down when moving
        ball.physicsBody?.angularDamping = 0.5  // slow down when spinning
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
