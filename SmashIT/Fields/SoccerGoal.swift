//
//  SoccerGoal.swift
//  SmashIT
//
//  Created by student on 24.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class SoccerGoal : Goal {
    
    let goalNetTexture: SKTexture
    
    init(position: CGPoint, goalImage: String, goalLine: String, goalNet: String, goalName: String) {
        self.goalNetTexture = SKTexture(imageNamed: goalNet)
        super.init(position: position, goalImage: goalImage, goalLine: goalLine, goalName: goalName)
        createNet()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func createNet() {
        let net = SKSpriteNode(texture: goalNetTexture)
        net.size = CalcSize.calcRelativeNodeSize(size: CGSize(width: goalNetTexture.size().width, height: goalNetTexture.size().height))
        net.zPosition = 2
        self.addChild(net)
    }
}
