//
//  GameStartDelayTimer.swift
//  SmashIT
//
//  Created by student on 29.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit

class GameStartDelayTimer {
    
    func setUp(worldNode: SKNode, gameScene: SKScene) {
        var background = SKSpriteNode()
        var remainingTime = 0
        var timeLabel = SKLabelNode()
        worldNode.isPaused = true
        background = SKSpriteNode(color: UIColor.black, size: CGSize(width: gameScene.size.width, height: gameScene.size.width))
        background.alpha = 0.7
        background.zPosition = 99
        gameScene.addChild(background)
        remainingTime = 3
        let timeInterval = SKAction.wait(forDuration: 1)
        timeLabel = SKLabelNode(text: "\(remainingTime)")
        timeLabel.position =  CGPoint(x: 0 , y: 0)
        timeLabel.fontSize = CalcSize.calcFontsize(fontSize: 180)
        timeLabel.fontColor = UIColor.white
        timeLabel.fontName = "AvenirNext-Bold"
        timeLabel.zPosition = 101
        gameScene.addChild(timeLabel)
        animateButton(button: timeLabel)
        let updateTimer = SKAction.run({
            remainingTime -= 1
            timeLabel.text = "\(remainingTime)"
            if remainingTime > 0 {
            } else {
                worldNode.isPaused = false
                gameScene.physicsWorld.speed = 1
                background.removeFromParent()
                timeLabel.removeFromParent()
            }
        })
        let sequence = SKAction.sequence([timeInterval,updateTimer])
        gameScene.run(SKAction.repeat(sequence, count: 3))
    }
    
    func animateButton(button: SKNode){
        let scaleUp = SKAction.scale(to: 1.40, duration: 0.5)
        let scaleDown = SKAction.scale(to: 1.0, duration: 0.5)
        
        let sequence = SKAction.sequence([scaleUp, scaleDown])
        button.run(SKAction.repeatForever(sequence))
    }
}
