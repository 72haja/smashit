//
//  GameCenterAchievement.swift
//  SmashIT
//
//  Created by Timo Kübler on 29.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import UIKit
import GameKit
class GameCenterAchievement: UIViewController {
    
    func setGoalAchievements(){
        self.setGoal1Achievment()
      self.setGoals_OverallAchievment(percentageComplete: 10.0, achievmentId: "achievementScorePoints10")
        // new ID= achievementScorePointsTen
       // self.setGoals_OverallAchievment(percentageComplete: 10.0, achievmentId: "achievementScorePoints100")
        // new ID = achievementScorePointsHundred
        
    }
    
    
    func setWinAchievements(){
        self.setWin1GameAchievment()
        self.setWin10GamesAchievment()
    }
    
    func setGamesPlayed(){
        self.setFirstGamePlayedAchievment()
        self.set5GamesPlayed_OverallAchievment(percentageComplete: 20.0, achievmentId: "achievementFiveGames")
    }
    
    func resetAchievments(){
        GKAchievement.resetAchievements(completionHandler: nil)
        print("Reset Achievments")
    }
    
    
    
    func setFirstGamePlayedAchievment(){
        let achievement = GKAchievement(identifier: "achievementFirstGame")
        achievement.percentComplete = 100
        GKAchievement.report([achievement], withCompletionHandler: nil)
        
    }
    
    
    
    func set5GamesPlayed_OverallAchievment(percentageComplete: Double!, achievmentId: String!){
 
        GKAchievement.loadAchievements() { achievements, error in
            guard let loadedAchievments = achievements else { return }
            print("Loaded Achievments: ")
            print (loadedAchievments)
            
            if(loadedAchievments.isEmpty == false){
                for achievment in loadedAchievments{
                    if(achievment.identifier == achievmentId){
                        let achievmentProgress = achievment.percentComplete
                        print("Progress For Achievement: " + achievmentId)
                        print(achievmentProgress)
                        let completed = percentageComplete + achievmentProgress

                        if(completed == 100.0){
                            let achievement = GKAchievement(identifier: achievmentId)
                            achievement.percentComplete = 100
                            achievement.showsCompletionBanner = true
                            GKAchievement.report([achievement], withCompletionHandler: nil)
                        }
                        else if(completed != 100.0){
                            let achievement = GKAchievement(identifier: achievmentId)
                            achievement.percentComplete = completed
                            GKAchievement.report([achievement], withCompletionHandler: nil)
                        }
                    }
                }
            }
            else if(loadedAchievments.isEmpty == true){
                print("Loaded Achievement was Empty. Setting First Time Achievment")
                let achievement = GKAchievement(identifier: achievmentId)
                achievement.percentComplete = percentageComplete
                GKAchievement.report([achievement], withCompletionHandler: nil)
            }
        }
    }
    
    
    
    func setGoal1Achievment(){
        let achievement = GKAchievement(identifier: "achievementScorePoints1")
        achievement.percentComplete = 100
        GKAchievement.report([achievement], withCompletionHandler: nil)
        
    }
    
    
    
    func setGoals_OverallAchievment(percentageComplete: Double!, achievmentId: String!){
        
        GKAchievement.loadAchievements() { achievements, error in
            guard let loadedAchievments = achievements else { return }
            if(loadedAchievments.isEmpty == false){
                for achievment in loadedAchievments{
                    if(achievment.identifier == achievmentId){
                        let achievmentProgress = achievment.percentComplete
                        print("Progress For Achievement: " + achievmentId)
                        print(achievmentProgress)
                        let completed = percentageComplete + achievmentProgress
                        
                        if(completed == 100.0){
                            let achievement = GKAchievement(identifier: achievmentId)
                            achievement.percentComplete = 100
                            achievement.showsCompletionBanner = true
                            GKAchievement.report([achievement], withCompletionHandler: nil)
                        }
                        else if(completed != 100.0){
                            let achievement = GKAchievement(identifier: achievmentId)
                            achievement.percentComplete = completed
                            GKAchievement.report([achievement], withCompletionHandler: nil)
                        }
                    }
                }
            }
            else if(loadedAchievments.isEmpty == true){
                let achievement = GKAchievement(identifier: achievmentId)
                achievement.percentComplete = percentageComplete
                GKAchievement.report([achievement], withCompletionHandler: nil)
            }
        }
        
    }
    
    
    
    
    func setGoals100_OverallAchievment(){
        let percentageComplete = 1.0
        GKAchievement.loadAchievements() { achievements, error in
            guard let loadedAchievments = achievements else { return }
            if(loadedAchievments.isEmpty == false){
                for achievment in loadedAchievments{
                    if(achievment.identifier == "achievementScorePoints100"){
                        let achievmentProgress = achievment.percentComplete
                        print("Progress For Achievement= Goals 100 Overall: " )
                        print(achievmentProgress)
                        let completed = percentageComplete + achievmentProgress
                        
                        if(completed == 100.0){
                            let achievement = GKAchievement(identifier: "achievementScorePoints100")
                            achievement.percentComplete = 100
                            achievement.showsCompletionBanner = true
                            GKAchievement.report([achievement], withCompletionHandler: nil)
                        }
                        else if(completed != 100.0){
                            let achievement = GKAchievement(identifier: "achievementScorePoints100")
                            achievement.percentComplete = completed
                            GKAchievement.report([achievement], withCompletionHandler: nil)
                        }
                    }
                }
            }
            else if(loadedAchievments.isEmpty == true){
                let achievement = GKAchievement(identifier: "achievementScorePoints100")
                achievement.percentComplete = percentageComplete
                GKAchievement.report([achievement], withCompletionHandler: nil)
            }
        }
    }
    
    
    
    
    func setWin1GameAchievment(){
        let percentageComplete = 100.0
        GKAchievement.loadAchievements() { achievements, error in
            guard let loadedAchievments = achievements else { return }
            if(loadedAchievments.isEmpty == false){
                for achievment in loadedAchievments{
                    if(achievment.identifier == "achievementWinGames1"){
                        let achievmentProgress = achievment.percentComplete
                        let completed = percentageComplete + achievmentProgress
                        
                        if(completed == 100.0){
                            let achievement = GKAchievement(identifier: "achievementWinGames1")
                            achievement.percentComplete = 100
                            achievement.showsCompletionBanner = true
                            GKAchievement.report([achievement], withCompletionHandler: nil)
                        }
                        else if(completed != 100.0){
                            let achievement = GKAchievement(identifier: "achievementWinGames1")
                            achievement.percentComplete = completed
                            GKAchievement.report([achievement], withCompletionHandler: nil)
                        }
                    }
                }
            }
            else if(loadedAchievments.isEmpty == true){
                let achievement = GKAchievement(identifier: "achievementWinGames1")
                achievement.percentComplete = percentageComplete
                GKAchievement.report([achievement], withCompletionHandler: nil)
            }
        }
    }
    
    
    
    func setWin10GamesAchievment(){
        let percentageComplete = 10.0
        GKAchievement.loadAchievements() { achievements, error in
            guard let loadedAchievments = achievements else { return }
            if(loadedAchievments.isEmpty == false){
                for achievment in loadedAchievments{
                    if(achievment.identifier == "achievementWinGames10"){
                        let achievmentProgress = achievment.percentComplete
                        let completed = percentageComplete + achievmentProgress
                        
                        if(completed == 100.0){
                            let achievement = GKAchievement(identifier: "achievementWinGames10")
                            achievement.percentComplete = 100
                            achievement.showsCompletionBanner = true
                            GKAchievement.report([achievement], withCompletionHandler: nil)
                        }
                        else if(completed != 100.0){
                            let achievement = GKAchievement(identifier: "achievementWinGames10")
                            achievement.percentComplete = completed
                            GKAchievement.report([achievement], withCompletionHandler: nil)
                        }
                    }
                }
            }
            else if(loadedAchievments.isEmpty == true){
                let achievement = GKAchievement(identifier: "achievementWinGames10")
                achievement.percentComplete = percentageComplete
                GKAchievement.report([achievement], withCompletionHandler: nil)
            }
        }
    }
    
    
    
    func setPlay90MinutesAchievment(){
        let achievement = GKAchievement(identifier: "achievementPlaying90Minutes")
        achievement.percentComplete = 100
       // achievement.showsCompletionBanner = true
        GKAchievement.report([achievement], withCompletionHandler: nil)
        
    }
}
