//
//  GameCenterHelper.swift
//  SmashIT
//
//  Created by student on 06.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
/*         GKMatchmaker.shared().findMatch(for: request, withCompletionHandler: {(match, error) -> Void in
 if error != nil {
 // Process the error.
 print("Error finding match")
 print(error?.localizedDescription)
 self.show_match_error();
 } */

import GameKit
import SpriteKit

public protocol GCHelperDelegate: class {
    
    /// Method called when a match has been initiated.
    func matchStarted()
    
    /// Method called when the device received data about the match from another device in the match.
    func match(_ match: GKMatch, didReceiveData: Data, fromPlayer: String)
    
    /// Method called when the match has ended.
    func matchEnded()
}


final class GameCenterHelper: NSObject, GKMatchDelegate, GKLocalPlayerListener  {
    fileprivate weak var delegate: GCHelperDelegate?
    var gcEnabled = Bool()
    var gcDefaultLeaderBoard = String()
    let LEADERBOARD_ID = "smashit.hft-stuttgart.leaderboard"
    var points = 0
    typealias CompletionBlock = (Error?) -> Void
    
    static let helper = GameCenterHelper()
    var matchStarted = false
    static var isAuthenticated: Bool {
        return GKLocalPlayer.local.isAuthenticated
    }
    
    
    var globalData = GlobalData()
    var powerUpData = PowerUpData()
    var nextRound = false
    var goalTeam1 = false
    var goalTeam2 = false
    var playerLeft = false
    
    var viewController: UIViewController?
    var currentMatchmakerVC: GKMatchmakerViewController?
    var currentMatchmaker : GKMatchmaker?
    var gameCenterviewController: GKGameCenterViewController?
    var localPlayer: GKLocalPlayer?
    var remotePlayer: GKPlayer?
    enum GameCenterHelperError: Error {
        case matchNotFound
    }
    var currentMatch: GKMatch?
    
    func authenticate() {
        localPlayer = GKLocalPlayer.local
        
        localPlayer!.authenticateHandler = {(ViewController, error) -> Void in
            if((ViewController) != nil) {
                // 1. Show login if player is not logged in
                self.viewController?.present(ViewController!, animated: true, completion: nil)
            } else if (self.localPlayer!.isAuthenticated) {
                self.gcEnabled = true
                self.gcDefaultLeaderBoard = self.LEADERBOARD_ID
                self.localPlayer!.loadDefaultLeaderboardIdentifier(completionHandler: { (LEADERBOARD_ID, error) in
                    if error != nil { print(error as Any)
                    } else { self.gcDefaultLeaderBoard = LEADERBOARD_ID! }
                })
                
            } else {
                self.gcEnabled = false
                print("Local player could not be authenticated!")
                print(error as Any)
            }
        }
    }
    
    func presentMatchmaker() {
        guard GKLocalPlayer.local.isAuthenticated else {
            return
        }
        let request = GKMatchRequest()
        request.minPlayers = 2
        request.maxPlayers = 2
        request.inviteMessage = "Would you like to play SmashIT?"
        let vc = GKMatchmakerViewController(matchRequest: request)
        vc!.matchmakerDelegate = self
        currentMatchmakerVC = vc
        viewController?.present(vc!, animated: true)
    }
    
    func showLeaderBoard() {
//        let gcVC = GKGameCenterViewController()
//        gcVC.gameCenterDelegate = self
//        gcVC.viewState = GKGameCenterViewControllerState.leaderboards
//        gcVC.leaderboardIdentifier = LEADERBOARD_ID
//        self.present(gcVC, animated: true, completion: nil)
       let bestScoreInt = GKScore(leaderboardIdentifier: LEADERBOARD_ID)
       bestScoreInt.value = Int64(10)
        GKScore.report([bestScoreInt]) { (error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                print("Best Score submitted to your Leaderboard!")
            }
        }
        
        let vc = GKGameCenterViewController()
        vc.gameCenterDelegate = self
        vc.viewState = GKGameCenterViewControllerState.leaderboards
        vc.leaderboardIdentifier = LEADERBOARD_ID
        gameCenterviewController = vc
        viewController?.present(vc, animated: true)
    }
    
   
    func showAchievements() {
        let vc = GKGameCenterViewController()
        vc.gameCenterDelegate = self
        vc.leaderboardIdentifier = LEADERBOARD_ID
        vc.viewState = GKGameCenterViewControllerState.achievements
        gameCenterviewController = vc
        viewController?.present(vc, animated: true)
    }
    
    
    
    public func match(_ theMatch: GKMatch, didReceive data: Data, fromPlayer playerID: String) {
        if currentMatch != theMatch {
            return
        }
        let decoder = JSONDecoder()
        if let decoded = try? decoder.decode(GlobalData.self, from: data) {
            globalData = decoded
            delegate?.match(theMatch, didReceiveData: data, fromPlayer: playerID)
        }
        else if let decoded = try? decoder.decode(PowerUpData.self, from: data) {
            powerUpData = decoded
        }
        else{
            print("Received Data: " + String(data: data, encoding: .utf8)!)
            if(String(data: data, encoding: .utf8)! == StateChange.nextRound.rawValue){
                print("nextRound set to true")
                nextRound = true
            }
            if(String(data: data, encoding: .utf8)! == StateChange.goalTeam1.rawValue){
                print("goalTeam1 set to true")
                goalTeam1 = true
            }
            if(String(data: data, encoding: .utf8)! == StateChange.goalTeam2.rawValue){
                print("goalTeam2 set to true")
                goalTeam2 = true
            }
            if(String(data: data, encoding: .utf8)! == StateChange.playerLeft.rawValue){
                print("player left")
                playerLeft = true
            }
        }
    }
    
    public func match(_ theMatch: GKMatch, player playerID: String, didChange state: GKPlayerConnectionState) {
        if currentMatch != theMatch {
            return
        }
        
        switch state {
        case GKPlayerConnectionState.connected where !matchStarted && theMatch.expectedPlayerCount == 0:
            print("something")
        case GKPlayerConnectionState.disconnected:
            matchStarted = false
            delegate?.matchEnded()
            currentMatch = nil
        default:
            break
        }
    }
    
    public func match(_ theMatch: GKMatch, didFailWithError error: Error?) {
        if currentMatch != theMatch {
            return
        }
        
        print("Match failed with error: \(String(describing: error?.localizedDescription))")
        matchStarted = false
        delegate?.matchEnded()
    }
}


extension GameCenterHelper: GKGameCenterControllerDelegate {
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
}

extension GameCenterHelper: GKMatchmakerViewControllerDelegate {
    func matchmakerViewControllerWasCancelled(_ viewController: GKMatchmakerViewController) {
        viewController.dismiss(animated: true)
    }
    
    func matchmakerViewController(_ viewController: GKMatchmakerViewController, didFailWithError error: Error) {
        print("Matchmaker vc did fail with error: \(error.localizedDescription).")
    }
    
    func matchmakerViewController(_ viewController: GKMatchmakerViewController, didFind match: GKMatch) {
        match.delegate = self
        currentMatch = match
        if currentMatch != nil {
            for p in currentMatch!.players{
                remotePlayer = p
            }
            NotificationCenter.default.post(name: .presentGame, object: currentMatch)
            viewController.dismiss(animated: true)
        }
    }
}

extension Notification.Name {
    static let presentGame = Notification.Name(rawValue: "presentGame")
    static let authenticationChanged = Notification.Name(rawValue: "authenticationChanged")
}
