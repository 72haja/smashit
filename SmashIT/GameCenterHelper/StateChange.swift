//
//  StateChange.swift
//  SmashIT
//
//  Created by Kai Widmaier on 16.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation

enum StateChange: String, Codable{
    case unknown = "UNKNOWN"
    case nextRound = "NEXT_ROUND"
    case goalTeam1 = "GOAL_TEAM1"
    case goalTeam2 = "GOAL_TEAM2"
    case playerLeft = "GAME_OVER"
}

