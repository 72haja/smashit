//
//  ProgressBar.swift
//  SmashIT
//
//  Created by student on 27.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class CountdownBar: SKSpriteNode{
    
    var width: Int
    var height: Int
    var time: Int
    
    init(width: Int, height: Int, time: Int, position: CGPoint){
        self.width = width
        self.height = height
        self.time = time
        super.init(texture: nil, color:SKColor.orange, size: CGSize(width: width, height: height))
        drawBorder(color: UIColor.darkGray, width: CGFloat(5))
        self.position = position
        self.anchorPoint = CGPoint(x: 0, y: 0.5)
        self.zPosition = 10
    }
    
    func startTimer(funcToCall: @escaping () -> ()){
        self.removeAction(forKey: "countdown") //If timer is already running cancel it
        resetBar()
        var timeValue = time
        let wait = SKAction.wait(forDuration: 1)
        let block = SKAction.run({
            if timeValue > 0{
                timeValue = timeValue - 1
                self.tick()
            }
            else{
                self.removeAction(forKey: "countdown")
                funcToCall()
            }
        })
        let sequence = SKAction.sequence([wait,block])
        run(SKAction.repeatForever(sequence), withKey: "countdown")
    }
    
    private func drawBorder(color: UIColor, width: CGFloat) {
        let shapeNode = SKShapeNode(rect: frame)
        shapeNode.fillColor = .clear
        shapeNode.strokeColor = color
        shapeNode.lineWidth = width
        shapeNode.zPosition = 10
        shapeNode.position.x += CGFloat(self.width / 2) //Workaround so border has same position as node
        addChild(shapeNode)
    }
    
    func tick(){
        if(self.size.width >= CGFloat(width / time)){
            self.size.width -= CGFloat(width / time)
        }
        else{
            self.size.width = 0
        }
    }
    
    func resetBar(){
        self.size = CGSize(width: width, height: height)
        self.removeAction(forKey: "countdown")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
