//
//  SameTeamDialog.swift
//  SmashIT
//
//  Created by student on 30.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class SameTeamDialog {
    
    
    var sameTeamTitel = SKLabelNode()
    var agreeButtonText = SKLabelNode()
    var background = SKSpriteNode()
    var agreeButton = SKSpriteNode()
    
    func createSameTeamDialog(scene: SKNode, width: CGFloat, height: CGFloat){
        
        background = SKSpriteNode(color: UIColor.lightGray, size: CGSize(width: width, height: height))
        background.alpha = 0.7
        background.zPosition = 99
        scene.addChild(background)
        
        sameTeamTitel.text = "You have chosen the same team. Please Change!"
        sameTeamTitel.preferredMaxLayoutWidth = width/2
        sameTeamTitel.numberOfLines = 2
        sameTeamTitel.position = CGPoint(x:0, y: 0)
        createLabel(label: sameTeamTitel, color: UIColor.white)
        scene.addChild(sameTeamTitel)
        
        agreeButton = SKSpriteNode(imageNamed: "Cancel_Template")
        agreeButtonText.text = "Got it!"
        agreeButton.position = CGPoint(x: 0, y: -60)
        createButton(button: agreeButton, color: UIColor.red, label: agreeButtonText)
        scene.addChild(agreeButton)
        
    }
    
    func createButton(button: SKSpriteNode, color: UIColor, label: SKLabelNode){
        button.size = CGSize(width: 400, height: 80)
        button.zPosition = 100
        label.fontColor = color
        label.fontName = "AvenirNext-Bold"
        label.fontSize = CGFloat(integerLiteral: 40)
        label.position = CGPoint(x: 0, y: -15)
        label.zPosition = 101
        button.addChild(label)
    }
    
    func createLabel(label: SKLabelNode, color: UIColor){
        label.fontColor = color
        label.fontName = "AvenirNext-Bold"
        label.fontSize = CGFloat(integerLiteral: 40)
        label.position = CGPoint(x: 0, y: -15)
        label.zPosition = 101
        
    }
    
    func removeDialog(){
        sameTeamTitel.removeFromParent()
        agreeButton.removeFromParent()
        background.removeFromParent()
    }
}
