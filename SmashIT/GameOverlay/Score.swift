//
//  Score.swift
//  SmashIT
//
//  Created by student on 28.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit

class Score: SKNode {
    
    let team1: Team
    let team2: Team
    var scoreTeam1 = SKLabelNode()
    var scoreTeam2 = SKLabelNode()
    var scoreTeam1Dialog = SKLabelNode()
    var scoreTeam2Dialog = SKLabelNode()
    var scoreDialog = SKSpriteNode()
    var scoreDialogTime: Int!
    
    init(team1: Team, team2: Team) {
        self.team1 = team1
        self.team2 = team2
        scoreDialogTime = 2
        super.init()
        
        initializeScore()
        initializeScoreDialog()
    }
    
    fileprivate func initializeScore(){
        scoreTeam1 = createScoreLabels(position: CalcSize.calcNodePosition(position: CGPoint(x: 0.125, y: 0.85)), zPosition: 0.5, fontsize: CalcSize.calcFontsize(fontSize: 70), parent: self)
        scoreTeam2 = createScoreLabels(position: CalcSize.calcNodePosition(position: CGPoint(x: -0.125, y: 0.85)), zPosition: 0.5, fontsize: CalcSize.calcFontsize(fontSize: 70), parent: self)
        
        createTeamIcon(team: team1, position: CalcSize.calcNodePosition(position: CGPoint(x: 0.125, y: 0.9)), zPosition: 0.2, alpha: 0.3, parent: self, overlay: true)
        createTeamIcon(team: team2, position: CalcSize.calcNodePosition(position: CGPoint(x: -0.125, y: 0.9)), zPosition: 0.2, alpha: 0.3, parent: self, overlay: true)
    }
    
    fileprivate func initializeScoreDialog(){
        scoreDialog = SKSpriteNode(color: UIColor.lightGray, size: CGSize(width: CalcSize.calcRelativeNodeWidth(width: 500), height: CalcSize.calcRelativeNodeWidth(width: 300)))
        scoreDialog.zPosition = 50
        scoreDialog.alpha = 0.8
        
        scoreTeam1Dialog = createScoreLabels(position: CalcSize.calcNodePosition(position: CGPoint(x: 0.12, y: -0.05)), zPosition: 100, fontsize: CalcSize.calcFontsize(fontSize: 75), parent: scoreDialog)
        scoreTeam2Dialog = createScoreLabels(position: CalcSize.calcNodePosition(position: CGPoint(x: -0.12, y: -0.05)), zPosition: 100, fontsize: CalcSize.calcFontsize(fontSize: 75), parent: scoreDialog)
        
        createTeamIcon(team: team1, position: CalcSize.calcNodePosition(position: CGPoint(x: 0.3, y: 0)), zPosition: 100, alpha: 1, parent: scoreDialog, overlay: false)
        createTeamIcon(team: team2, position: CalcSize.calcNodePosition(position: CGPoint(x: -0.29, y: 0)), zPosition: 100, alpha: 1, parent: scoreDialog, overlay: false)
    }
    
    fileprivate func createScoreLabels(position: CGPoint, zPosition: CGFloat, fontsize: CGFloat, parent: SKNode) -> SKLabelNode {
        let team = SKLabelNode(text: "0")
        team.zPosition = zPosition
        team.fontSize = fontsize
        team.fontName = "AvenirNext-Bold"
        team.position = position
        parent.addChild(team)
        return team
    }
    
    fileprivate func createTeamIcon(team: Team, position: CGPoint, zPosition: CGFloat, alpha: CGFloat, parent: SKNode, overlay: Bool) {
        let icon = SKSpriteNode(imageNamed: team.name.uppercased())
        icon.size = CalcSize.calcRelativeNodeSizeForSqare(size: icon.size.width)
        icon.position = position
        icon.zPosition = zPosition
        icon.alpha = alpha
        parent.addChild(icon)
        if overlay {
            createGrayOverlay(for: icon)
        }
    }
    
    fileprivate func createGrayOverlay(for icon: SKSpriteNode) {
        let grayBackground = SKSpriteNode(color: .gray, size: icon.size)
        grayBackground.position = icon.position
        grayBackground.zPosition = 0.1
        grayBackground.alpha = 0.7
        addChild(grayBackground)
    }

    func showScore(nodeToPause: SKNode) {
        createScoreDialog(nodeToPause)
        scoreTeam1.text = "\(team1.score)"
        scoreTeam2.text = "\(team2.score)"
    }
    
    func showFinalScoreDialog(nodeToPause: SKNode, winner: String){
        scoreDialogTime = 5
        let gameOverLabel = createScoreLabels(position: CalcSize.calcNodePosition(position: CGPoint(x: 0, y: 0.2)), zPosition: 100, fontsize: CalcSize.calcFontsize(fontSize: 75), parent: scoreDialog)
        gameOverLabel.text = "GAME OVER"
        let winnerLabel = createScoreLabels(position: CalcSize.calcNodePosition(position: CGPoint(x: 0, y: -0.3)), zPosition: 100, fontsize: CalcSize.calcFontsize(fontSize: 50), parent: scoreDialog)
        if winner == team1.name {
            winnerLabel.text = "Winner: \(team1.name.capitalized)"
        } else if winner == "both" {
            winnerLabel.text = "It's a draw"
        } else {
            winnerLabel.text = "Winner: \(team2.name.capitalized)"
        }
        
        showScore(nodeToPause: nodeToPause)
    }
    
    fileprivate func createScoreDialog(_ nodeToPause: SKNode) {
        nodeToPause.isPaused = true
        addChild(scoreDialog)
        let label = createScoreLabels(position: CalcSize.calcNodePosition(position: CGPoint(x: 0.005, y: -0.04)), zPosition: 100, fontsize: CalcSize.calcFontsize(fontSize: 75), parent: scoreDialog)
        label.text = "-"
        scoreTeam1Dialog.text = "\(team1.score)"
        scoreTeam2Dialog.text = "\(team2.score)"
        
        // remove score dialog after scoreDialogTime
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(scoreDialogTime), execute: {
            self.scoreDialog.removeFromParent()
            nodeToPause.isPaused = false
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
