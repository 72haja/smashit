//
//  SurrenderDialog.swift
//  SmashIT
//
//  Created by student on 30.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class SurrenderDialog {
    
    var cancelButton = SKSpriteNode()
    var cancelButtonText = SKLabelNode()
    var surrenderButton = SKSpriteNode()
    var surrenderButtonText = SKLabelNode()
    var background = SKSpriteNode()
    
    func animateButton(button: SKSpriteNode){
        let scaleUp = SKAction.scale(to: 1.05, duration: 0.5)
        let scaleDown = SKAction.scale(to: 1.0, duration: 0.5)
        
        let sequence = SKAction.sequence([scaleUp, scaleDown])
        button.run(SKAction.repeatForever(sequence))
    }
    
    func createSurrenderDialog(scene: SKNode, width: CGFloat, height: CGFloat){
        background = SKSpriteNode(color: UIColor.lightGray, size: CGSize(width: CalcSize.calcRelativeNodeWidth(width: width), height: CalcSize.calcRelativeNodeWidth(width: height)))
        background.alpha = 0.7
        background.zPosition = 99
        scene.addChild(background)
        
        cancelButton = SKSpriteNode(imageNamed: "Cancel_Template")
        cancelButtonText.text = "Back to Game"
        cancelButton.position = CalcSize.calcNodePosition(position: CGPoint(x: 0, y: -0.15))
        createButton(button: cancelButton, color: UIColor.red, label: cancelButtonText)
        scene.addChild(cancelButton)
        
        surrenderButton = SKSpriteNode(imageNamed: "Surrender_Template")
        surrenderButtonText.text = "Surrender"
        surrenderButton.position = CalcSize.calcNodePosition(position: CGPoint(x: 0, y: 0.15))
        createButton(button: surrenderButton, color: UIColor.white, label: surrenderButtonText)
        scene.addChild(surrenderButton)
        animateButton(button: surrenderButton)
    }
    
    func createButton(button: SKSpriteNode, color: UIColor, label: SKLabelNode){
        button.size = CGSize(width: CalcSize.calcRelativeNodeWidth(width: 400), height: CalcSize.calcRelativeNodeWidth(width: 80))
        button.zPosition = 100
        label.fontColor = color
        label.fontName = "AvenirNext-Bold"
        label.fontSize = CalcSize.calcFontsize(fontSize: 50)
        label.position = CGPoint(x: 0, y: -15)
        label.zPosition = 101
        button.addChild(label)
    }
    
    func removeDialog(){
        cancelButton.removeFromParent()
        surrenderButton.removeFromParent()
        background.removeFromParent()
    }
}
