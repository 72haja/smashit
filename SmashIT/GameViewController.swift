//
//  GameViewController.swift
//  SmashIT
//
//  Created by student on 08.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class GameViewController: UIViewController {
    var gameConfig: GameConfig?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view = SKView(frame: view.bounds)
        if let view = self.view as! SKView? {
            let launchScreen = LaunchScreenScene(size: view.bounds.size)
            launchScreen.scaleMode = .aspectFill
            view.ignoresSiblingOrder = true
            view.presentScene(launchScreen)
            GameCenterHelper.helper.viewController = self
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredScreenEdgesDeferringSystemGestures: UIRectEdge {
        return [.all]
    }
}
