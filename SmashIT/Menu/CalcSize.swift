//
//  CalcSize.swift
//  SmashIT
//
//  Created by student on 21.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class CalcSize {
    
    static var frameSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    
    class func calcFontsize(fontSize: CGFloat) -> CGFloat {
        return (fontSize / 1000) * frameSize.height
    }
    
    class func calcNodePosition(position: CGPoint) -> CGPoint {
        return CGPoint(x: frameSize.width / 2 * position.x, y: frameSize.height / 2 * position.y)
    }
    
    class func calcRelativeNodeSize(size: CGSize) -> CGSize {
        return CGSize(width: CalcSize.calcRelativeNodeWidth(width: size.width), height: CalcSize.calcRelativeNodeHeight(height: size.height))
    }
    
    class func calcRelativeNodeSizeForSqare(size: CGFloat) -> CGSize {
        return CGSize(width: CalcSize.calcRelativeNodeWidth(width: size), height: CalcSize.calcRelativeNodeWidth(width: size))
    }
    
    class func calcRelativeNodeWidth(width: CGFloat) -> CGFloat {
        return  frameSize.width * (width / 1000)
    }
    
    class func calcRelativeNodeHeight(height: CGFloat) -> CGFloat {
        return frameSize.height * (height / 1000)
    }
}
