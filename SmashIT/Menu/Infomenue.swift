//
//  MatchSettingsScene.swift
//  SmashIT
//
//  Created by student on 03.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit


class Infomenue: SKScene {
    var btnBack = SKLabelNode()
    var backgroundLabel = SKLabelNode()
    var powerupLabel = SKLabelNode()
    let shape = SKShapeNode()
    
    var powerupSelector: ItemSelector!
    
    override func didMove(to view: SKView) {
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
        setupElements()
    }
    
    fileprivate func setupElements() {
        createBackButton()
        powerupSelector = createPowerupSelector(
            size: CGSize(width: frame.size.width , height: frame.size.height / 2.8),
            position: CGPoint(x: 0, y: frame.size.height/20 * 4), title: "PowerUPs")
        setupSeparators()
        createBackgroundLabel()
        createPowerUpLabel()
    }
    
    fileprivate func setupSeparators() {
        let teamBottomBar = SKSpriteNode(color: UIColor.gray, size: CGSize(width: frame.size.width, height: 1))
        teamBottomBar.position = CGPoint(x: 0, y: -frame.size.height/20 * 1)
        addChild(teamBottomBar)
    }
    
    //Klasse welche den Hintergrund für das Label erstellt welches das PowerUP erklärt
    fileprivate func createBackgroundLabel(){
        shape.path = UIBezierPath(roundedRect: CGRect(x: -(frame.size.width/20 * 15)/2, y: -frame.size.height / 20 * 9, width: frame.size.width/20 * 15, height: frame.size.height/20 * 7), cornerRadius: 10).cgPath
        shape.position = CGPoint(x: frame.midX, y: frame.midY)
        shape.fillColor = UIColor(red: 0.4078, green: 0.4078, blue: 0.4078, alpha: 1.0)
        shape.strokeColor = UIColor.white
        shape.lineWidth = 1
        addChild(shape)
    }
    
    fileprivate func createPowerUpLabel(){
        powerupLabel.position = CalcSize.calcNodePosition(position: CGPoint(x: 0, y: -0.65))
        powerupLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        powerupLabel.numberOfLines = 100
        powerupLabel.fontSize = CalcSize.calcFontsize(fontSize: 45)
        powerupLabel.preferredMaxLayoutWidth = shape.frame.width / 20 * 19
        addChild(powerupLabel)
        //erstes ITem übergeben sonst ist Textbox leer
        onMenuItemChanged(item: SelectableImageItem(text: "Green Snail", image: "snailGreen"))
    }
    
    fileprivate func createBackButton() {
        btnBack.text = "Back"
        btnBack.fontSize = CalcSize.calcFontsize(fontSize: 40)
        btnBack.position = CalcSize.calcNodePosition(position: CGPoint(x: -0.9, y: 0.9))
        addChild(btnBack)
    }
    
    private func createPowerupSelector(size: CGSize, position: CGPoint, title: String) -> ItemSelector {
        var powerup: [SelectableImageItem] = []
        powerup.append(SelectableImageItem(text: "Green Snail", image: "snailGreen"))
        powerup.append(SelectableImageItem(text: "Red Snail", image: "snailRed"))
        powerup.append(SelectableImageItem(text: "Resize Player", image: "resizePowerUp"))
        powerup.append(SelectableImageItem(text: "Timeout", image: "twoMinutes"))
        powerup.append(SelectableImageItem(text: "Phantom", image: "phantom"))
        powerup.append(SelectableImageItem(text: "Fog", image: "fogPowerUp"))
        powerup.append(SelectableImageItem(text: "Obstacle", image: "obstaclePowerUp"))
        powerup.append(SelectableImageItem(text: "Exchange Player", image: "exchangePlayer"))
        let selector = ItemSelector(size: size, itemDistance: 1, items: powerup, title: title, horizontal: true)
        selector.position = position
        selector.setOnItemChangeListener(callback: onMenuItemChanged(item:))
        addChild(selector)
        return selector
    }
    
    func onMenuItemChanged(item: ItemSelector.Item) {
        let removeNode = SKAction.fadeIn(withDuration: 1.0)
        
            switch(powerupSelector.currentItem.value as? String)
            {
                case("snailGreen"):
                    powerupLabel.text = "This PowerUP reduces your opponents movement speed."
                case("snailRed"):
                    powerupLabel.text = "This PowerUP reduces your own movement speed. Be careful!"
                case("resizePowerUp"):
                    powerupLabel.text = "This PowerUP increases the size of your players."
                case("twoMinutes"):
                    powerupLabel.text = "This PowerUP removes one of your opponents players for 7 seconds."
                case("phantom"):
                    powerupLabel.text = "This PowerUp makes it impossiple for your opponents players to touch the ball for 7 seconds."
                case("fogPowerUp"):
                    powerupLabel.text = "This PowerUP makes fog appear on the game field.  "
                case("obstaclePowerUp"):
                    powerupLabel.text = "This PowerUP spawns an obstacle with whom players can collide."
                case("exchangePlayer"):
                    powerupLabel.text = "This PowerUP exchanges your players position with one of your opponents."
                default:
                    powerupLabel.text = "Default"
            }
        powerupLabel.run(removeNode)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = (touches.first?.location(in: self))!
        if powerupLabel.contains(location){
           powerupLabel.text = powerupSelector.currentItem.value as? String
        }
        if btnBack.contains(location) {
            AVAudioPlayerPool.stop()
            AVAudioPlayerPool.playButtonClickSound()
            let startMenu = StartMenuScene(size: view!.bounds.size)
            startMenu.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            view!.presentScene(startMenu)
        }
    }
}
