//
//  MusicSoundElements.swift
//  SmashIT
//
//  Created by student on 24.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class MusicSoundElements: SKNode {
    
    let helper = CreateNodeHelper()
    var soundIcon = SKSpriteNode()
    var muteIcon = SKSpriteNode()
    var musicIcon = SKSpriteNode()
    var musicMuteIcon = SKSpriteNode()
    
    let storeValues = StoreValue()
    let soundIconKey = "iconKey"
    let musicIconKey = "musicIconKey"
    
    let iconSize: CGFloat = 60
    
    override init() {
        super.init()
        
        isUserInteractionEnabled = true
        createMusicAndMuteIcon()
        createSoundAndMuteIcon()
        loadBackgroundMusik()
    }
    
    fileprivate func createMusicAndMuteIcon() {
        musicIcon = helper.createSpriteNode(imageName: "musicIcon", size: CalcSize.calcRelativeNodeSizeForSqare(size: iconSize), position: CalcSize.calcNodePosition(position: CGPoint(x: 0.88, y: 0.84)), zPosition: 1, physicsBody: false, bodyRadius: 0)
        musicMuteIcon = helper.createSpriteNode(imageName: "musicMuteIcon", size: CalcSize.calcRelativeNodeSizeForSqare(size: iconSize), position: CalcSize.calcNodePosition(position: CGPoint(x: 0.88, y: 0.84)), zPosition: 1, physicsBody: false, bodyRadius: 0)
        
        if storeValues.retrieveIconValue(soundIconActive: self.children.contains(musicIcon), key: musicIconKey) == "sound" {
            addChild(musicIcon)
            AVAudioPlayerPool.setGlobalVolume(volume: 1.0, sound: false)
        } else {
            addChild(musicMuteIcon)
            AVAudioPlayerPool.musicMute()
        }
    }
    
    fileprivate func createSoundAndMuteIcon() {
        soundIcon = helper.createSpriteNode(imageName: "Speaker_Icon", size: CalcSize.calcRelativeNodeSizeForSqare(size: (iconSize - 10)), position: CalcSize.calcNodePosition(position: CGPoint(x: 0.71, y: 0.84)), zPosition: 1, physicsBody: false, bodyRadius: 0)
        
        muteIcon = helper.createSpriteNode(imageName: "Mute_Icon", size: CalcSize.calcRelativeNodeSizeForSqare(size: (iconSize - 10)), position: CalcSize.calcNodePosition(position: CGPoint(x: 0.71, y: 0.84)), zPosition: 1, physicsBody: false, bodyRadius: 0)
        
        if storeValues.retrieveIconValue(soundIconActive: self.children.contains(soundIcon), key: soundIconKey) == "sound" {
            addChild(soundIcon)
            AVAudioPlayerPool.setGlobalVolume(volume: 1.0, sound: true)
        } else {
            addChild(muteIcon)
            AVAudioPlayerPool.soundMute()
        }
    }
    
    fileprivate func showSoundIcon() {
        storeValues.saveIconValue(iconValue: "sound", key: soundIconKey)
        muteIcon.removeFromParent()
        addChild(soundIcon)
        AVAudioPlayerPool.setGlobalVolume(volume: 1.0, sound: true)
        AVAudioPlayerPool.playButtonClickSound()
        
    }
    
    fileprivate func showSoundMuteIcon() {
        storeValues.saveIconValue(iconValue: "mute", key: soundIconKey)
        soundIcon.removeFromParent()
        addChild(muteIcon)
        AVAudioPlayerPool.soundMute()
    }
    
    fileprivate func showMusicIcon() {
        storeValues.saveIconValue(iconValue: "sound", key: musicIconKey)
        musicMuteIcon.removeFromParent()
        addChild(musicIcon)
        AVAudioPlayerPool.setGlobalVolume(volume: 1.0, sound: false)
        AVAudioPlayerPool.playButtonClickSound()
    }
    
    fileprivate func showMusicMuteIcon() {
        storeValues.saveIconValue(iconValue: "mute", key: musicIconKey)
        musicIcon.removeFromParent()
        addChild(musicMuteIcon)
        AVAudioPlayerPool.musicMute()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = (touches.first?.location(in: self))!
        if self.children.contains(soundIcon) && soundIcon.contains(location){
            showSoundMuteIcon()
        } else if self.children.contains(muteIcon) && muteIcon.contains(location){
            showSoundIcon()
        } else if self.children.contains(musicIcon) && musicIcon.contains(location) {
            showMusicMuteIcon()
        } else if self.children.contains(musicMuteIcon) && musicMuteIcon.contains(location) {
            showMusicIcon()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadBackgroundMusik(){
        if let url = Bundle.main.url(forResource: "menu_background_1_0", withExtension: "mp3") {
            let backgroundMusik = AVAudioPlayerPool.player(url: url, sound: false)!
            backgroundMusik.audioPlayer.numberOfLoops = -1
            backgroundMusik.audioPlayer.play()
        }
    }
    
}

