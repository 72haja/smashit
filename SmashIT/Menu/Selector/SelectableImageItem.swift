//
//  SelectableImageItem.swift
//  SmashIT
//
//  Created by student on 09.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit

class SelectableImageItem: ItemSelector.Item {
    
    var itemImage = SKSpriteNode()
    var itemLabel = SKLabelNode()
    var imageName: String
    var itemText: String
    
    init(text: String, image: String) {
        self.imageName = image
        self.itemText = text
        super.init(value: image)
        createImage(image)
        createLabel()
    }
    
    init(value: Any, text: String, image: String) {
        self.imageName = image
        self.itemText = text
        super.init(value: value)
        createImage(image)
        createLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createImage(_ image: String) {
        itemImage = SKSpriteNode(imageNamed: image)
        itemImage.position = CGPoint(x: 0, y: 0)
        itemImage.zPosition = 1
        addChild(itemImage)
    }
    
    private func createLabel() {
        itemLabel = SKLabelNode(text: itemText)
        itemLabel.fontSize = CalcSize.calcFontsize(fontSize: 40)
        itemLabel.position = CGPoint(x: 0, y: 0)
        itemLabel.zPosition = 2
        addChild(itemLabel)
    }
    
    override func setItemSize(height: CGFloat) {
        let labelHeight = itemLabel.frame.size.height
        itemLabel.position = CGPoint(x: 0, y: (-height / 2))
        
        let imageHeight = height - (2.2 * labelHeight)
        let aspectRatio = itemImage.size.width / itemImage.size.height
        itemImage.size = CGSize(width: imageHeight * aspectRatio, height: imageHeight)
        itemImage.position = CGPoint(x: 0, y: labelHeight)
    }
    
    override func highlightItem() {
        itemImage.run(SKAction.scale(to: 1.3, duration: 0.5))
    }
    
    override func normalizeItem() {
        itemImage.run(SKAction.scale(to: 1.0, duration: 0.5))
    }
    
    override func getItemSize(width: CGFloat) -> CGSize {
        return CGSize(width: itemImage.size.width + (0.06 * width), height: itemImage.size.height)
    }
}
