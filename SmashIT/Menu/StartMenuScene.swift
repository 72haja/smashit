//
//  StartMenuScene.swift
//  SmashIT
//
//  Created by student on 08.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit
import GameKit

class StartMenuScene: SKScene {

    var btnPlayMatch = SKLabelNode()
    var btnFindMatch = SKLabelNode()
    
    var btnAchievments = SKLabelNode()
    var resetAchievment = SKLabelNode()
    let achievementGameCenter = GameCenterAchievement()
    
    var btnInfoMenu = SKLabelNode()
    
    override func didMove(to view: SKView) {
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
        setupElements()
        GameCenterHelper.helper.authenticate()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(presentGame(_:)),
            name: .presentGame,
            object: nil
        )
    }
    
    fileprivate func setupElements() {
        createMusicLabel()
        createAppIconNode()
        createPlayLabel()
        createFindMatchLabel()
        createAchievmentLabel()
        createPowerUpLabel()
    }
    
    private func createMusicLabel() {
        let musicSoundElements = MusicSoundElements()
        addChild(musicSoundElements)
    }
    
    private func createAppIconNode() {
        let appIconNode = SKSpriteNode(imageNamed: "menu_app_icon")
        appIconNode.position = CalcSize.calcNodePosition(position: CGPoint(x: 0, y: 0.5))
        appIconNode.size = CGSize(width: size.width / 4, height: size.height / 3)
        addChild(appIconNode)
    }
    
    private func createPlayLabel() {
        btnPlayMatch = createLabel(text: "Play", position: CGPoint(x: 0, y: -0.1), fontSize: 65)
        addChild(btnPlayMatch)
    }
    
    private func createFindMatchLabel() {
        btnFindMatch = createLabel(text: "Find Match", position: CGPoint(x: 0, y: -0.3), fontSize: 65)
        btnFindMatch.color = .gray
        addChild(btnFindMatch)
    }
    
    private func createAchievmentLabel() {
        btnAchievments = createLabel(text: "Achievements", position: CGPoint(x: 0, y: -0.5), fontSize: 40)
        btnAchievments.color = .gray
        addChild(btnAchievments)
    }
    
    private func createPowerUpLabel() {
        btnInfoMenu = createLabel(text: "PowerUp's", position: CGPoint(x: 0, y: -0.7), fontSize: 40)
        addChild(btnInfoMenu)
    }
    
    private func createLabel(text: String, position: CGPoint, fontSize: CGFloat) -> SKLabelNode {
        let label = SKLabelNode(text: text)
        label.position = CalcSize.calcNodePosition(position: position)
        label.zPosition = 1
        label.fontSize = CalcSize.calcFontsize(fontSize: fontSize)
        label.fontName = "AvenirNext-Bold"
        label.fontColor = .white
        return label
    }
    
    @objc private func presentGame(_ notification: Notification) {
        guard let match = notification.object as? GKMatch else {
            return
        }
        loadAndDisplay(match: match)
    }
    
    private func loadAndDisplay(match: GKMatch) {
        let matchSettingsLobbyScene = MatchSettingsLobbyScene(size: view!.bounds.size)
        matchSettingsLobbyScene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        view!.presentScene(matchSettingsLobbyScene)
    }
    
    override func update(_ currentTime: TimeInterval) {
        if (GameCenterHelper.isAuthenticated) {
            btnFindMatch.color = SKColor.white
            btnAchievments.color = SKColor.white
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = (touches.first?.location(in: self))!
        if btnPlayMatch.contains(location){
            AVAudioPlayerPool.stop()
            let matchSettingsScene = MatchSettingsScene(size: view!.bounds.size)
            matchSettingsScene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            view!.presentScene(matchSettingsScene)
        } else if btnFindMatch.contains(location) && GameCenterHelper.isAuthenticated {
            AVAudioPlayerPool.stop()
            AVAudioPlayerPool.playButtonClickSound()
            GameCenterHelper.helper.presentMatchmaker()
        } else if btnInfoMenu.contains(location){
            let infomenue = Infomenue(size: view!.bounds.size)
            infomenue.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            view!.presentScene(infomenue)
        } else if btnAchievments.contains(location) && GameCenterHelper.isAuthenticated {
            AVAudioPlayerPool.stop()
            AVAudioPlayerPool.playButtonClickSound()
            GameCenterHelper.helper.showAchievements()
            //GKAchievement.resetAchievements(completionHandler: nil)
            
        }
    }
}
