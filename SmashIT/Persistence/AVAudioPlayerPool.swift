//
//  AVAudioPlayerPool.swift
//  SmashIT
//
//  Created by Erik Wolf on 16.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import AVFoundation
// An array of all players stored in the pool; not accessible
// outside this file
private var players : [AudioPlayer] = []
private var globalSoundVolume : Float = 0.0
private var globalMusicVolume : Float = 0.0
private var btnClick = AudioPlayer(url: Bundle.main.url(forResource: "button_click_1_1", withExtension: "mp3")!, sound: true)


class AVAudioPlayerPool: NSObject {
    
    // Given the URL of a sound file, either create or reuse an audio player
    class func player(url : URL, sound : Bool) -> AudioPlayer? {
        
        // Try and find a player that can be reused and is not playing
        let availablePlayers = players.filter { (player) -> Bool in
            return player.audioPlayer.isPlaying == false && player.audioPlayer.url == url
        }
        
        // If we found one, return it
        if let playerToUse = availablePlayers.first {
            print("Reusing player for \(url.lastPathComponent)")
            return playerToUse
        }
        
        // Didn't find one? Create a new one
        
        
        let newPlayer = AudioPlayer(url: url, sound: sound)
        players.append(newPlayer)
        if(newPlayer.issound){
            newPlayer.audioPlayer.volume = globalSoundVolume
        }
        else{
            newPlayer.audioPlayer.volume = globalMusicVolume
        }
        return newPlayer
    }
    
    class func musicMute(){
        setGlobalVolume(volume: 0.0, sound: false)
    }
    
    class func soundMute(){
        setGlobalVolume(volume: 0.0, sound: true)
        btnClick.audioPlayer.setVolume(globalSoundVolume, fadeDuration: 0)
    }
    
    class func stop(){
        for i in players{
            i.audioPlayer.stop()
        }
    }
    
    class func setGlobalVolume(volume: Float, sound: Bool){
        for i in players{
            if(sound == i.issound){
                i.audioPlayer.setVolume(volume, fadeDuration: 0)
            }
        }
        if(sound){
            globalSoundVolume = volume
            btnClick.audioPlayer.setVolume(globalSoundVolume, fadeDuration: 0)
        }
        else{
            globalMusicVolume = volume
        }
    }
    
    class func getSoundVolume() -> Float{
        return globalSoundVolume
    }
    
    class func getMusicVolume() -> Float{
        return globalMusicVolume
    }
    
    class func soundIsMuted() -> Bool{
        if globalSoundVolume.isEqual(to: 0.0){
            return true
        }
        else{
            return false
        }
    }
    
    class func playButtonClickSound(){
        btnClick.audioPlayer.play()
    }
}
