//
//  ObstaclePowerUp.swift
//  SmashIT
//
//  Created by student on 03.06.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class ObstaclePowerUp: PowerUp {
    
    var obstacle = SKSpriteNode()
    let obstacleCategory: UInt32 = 0b1 << 6
    
    init(scene: GameScene) {
        let textureName = "obstaclePowerUp"
        let upTime: TimeInterval =  5
        super.init(key: "ObstaclePowerUp", powerUpTextureName: textureName, scene: scene, upTime: upTime, affects: Affects.NONE)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func doAction(pickedUpBy: [Player], otherPlayers otherTeam: [Player]) {
        super.doAction(pickedUpBy: pickedUpBy, otherPlayers: otherTeam)
        createObstacle()
        
        for i in 0..<pickedUpBy.count {
            pickedUpBy[i].physicsBody!.collisionBitMask = GameScene.playerCategory | GameScene.ballCategory | GameScene.wallCategory | obstacleCategory
            otherTeam[i].physicsBody!.collisionBitMask = GameScene.playerCategory | GameScene.ballCategory | GameScene.wallCategory | obstacleCategory
        }
        gameScene.ball.physicsBody?.collisionBitMask = GameScene.playerCategory | GameScene.wallCategory | obstacleCategory
        
        setObstaclePositionAndMovement(position: CGPoint(x: -1.1, y: -1.1))
    }
    
    override func revertAction() {
        super.revertAction()
        obstacle.removeAllActions()
        obstacle.removeFromParent()
        
        for i in 0..<pickedUpBy!.count{
            pickedUpBy![i].physicsBody!.collisionBitMask = GameScene.playerCategory | GameScene.ballCategory | GameScene.wallCategory
            otherPlayers![i].physicsBody!.collisionBitMask = GameScene.playerCategory | GameScene.ballCategory | GameScene.wallCategory
        }
        gameScene.ball.physicsBody?.collisionBitMask = GameScene.playerCategory | GameScene.wallCategory
    }
    
    fileprivate func createObstacle() {
        obstacle = SKSpriteNode(imageNamed: MatchSettingsScene.teamNames[8])
        obstacle.size = CalcSize.calcRelativeNodeSizeForSqare(size: 100)
        obstacle.zPosition = 1
        
        obstacle.physicsBody = SKPhysicsBody(circleOfRadius: obstacle.size.width / 2)
        obstacle.physicsBody?.allowsRotation = false
        obstacle.physicsBody?.mass = 0.8
        obstacle.physicsBody?.categoryBitMask = obstacleCategory
        obstacle.physicsBody?.contactTestBitMask =  GameScene.noCategory
        obstacle.physicsBody?.collisionBitMask = GameScene.noCategory
    }
    
    fileprivate func setObstaclePositionAndMovement(position: CGPoint) {
        setStartPositionOfObstacle(position: position)
        moveObstacle(targetPoint: CGPoint(x: 275, y: 275), sndTargetPoint: CGPoint(x: -367, y: 367))
    }
    
    fileprivate func setStartPositionOfObstacle(position: CGPoint) {
        obstacle.position = CalcSize.calcNodePosition(position: position)
        gameScene.worldNode.addChild(obstacle)
    }
    
    fileprivate func moveObstacle(targetPoint: CGPoint, sndTargetPoint: CGPoint) {
        let firstImpulse = SKAction.applyImpulse(CGVector(dx: CalcSize.calcRelativeNodeWidth(width:targetPoint.x), dy: CalcSize.calcRelativeNodeHeight(height: targetPoint.y)), duration: 2)
        let secondImpulse = SKAction.applyImpulse(CGVector(dx: CalcSize.calcRelativeNodeWidth(width: sndTargetPoint.x), dy: CalcSize.calcRelativeNodeHeight(height: sndTargetPoint.y)), duration: 2)

        let actions = SKAction.sequence([firstImpulse, secondImpulse])
        obstacle.run(actions)
    }
}
