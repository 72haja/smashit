//
//  PhantomBallPowerUp.swift
//  SmashIT
//
//  Created by Erik Wolf on 03.06.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation

class PhantomBallPowerUp: PowerUp {
    let phantom: UInt32 = 0b1 << 6

    
    init(scene: GameScene) {
        let textureName = "phantom"
        let upTime: TimeInterval = 7
        super.init(key: "phantom", powerUpTextureName: textureName, scene: scene, upTime: upTime, affects: Affects.OTHER_TEAM)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func doAction(pickedUpBy: [Player], otherPlayers: [Player]) {
        super.doAction(pickedUpBy: pickedUpBy, otherPlayers: otherPlayers)
        for i in pickedUpBy{
            i.physicsBody?.collisionBitMask = GameScene.playerCategory | GameScene.ballCategory | GameScene.wallCategory | phantom
        }
        for i in otherPlayers{
            i.physicsBody?.categoryBitMask = phantom
            i.physicsBody?.collisionBitMask = GameScene.playerCategory | GameScene.wallCategory | phantom
        }
    }
    
    override func revertAction() {
        super.revertAction()
        for i in pickedUpBy!{
            i.physicsBody?.collisionBitMask = GameScene.playerCategory | GameScene.ballCategory | GameScene.wallCategory
        }
        for i in otherPlayers!{
            i.physicsBody?.categoryBitMask = GameScene.playerCategory
            i.physicsBody?.collisionBitMask = GameScene.playerCategory | GameScene.wallCategory | GameScene.ballCategory
        }
    }
}
