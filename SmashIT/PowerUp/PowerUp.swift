//
//  PowerUp.swift
//  SmashIT
//
//  Created by Svenja on 21.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class PowerUp: SKSpriteNode {
    
    enum Affects {
        case PICKED_UP_TEAM
        case OTHER_TEAM
        case NONE
    }
    
    private let key: String
    let powerUpTexture: SKTexture
    let upTime: TimeInterval!
    var isSpawned: Bool = false
    var isActive: Bool = false
    var gameScene: GameScene
    let affects: Affects
    
    var pickedUpBy: [Player]?
    var otherPlayers: [Player]?
    
    init(key: String, powerUpTextureName: String, scene: GameScene, upTime: TimeInterval, affects: Affects) {
        self.key = key
        self.powerUpTexture = SKTexture(imageNamed: powerUpTextureName)
        self.gameScene = scene
        self.upTime = upTime
        self.affects = affects
        super.init(texture: powerUpTexture, color: UIColor.clear, size: CGSize(width: CalcSize.calcRelativeNodeWidth(width: powerUpTexture.size().width), height: CalcSize.calcRelativeNodeWidth(width: powerUpTexture.size().width)))
        setUpPowerUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpPowerUp() {
        zPosition = 1
        physicsBody = SKPhysicsBody(texture: powerUpTexture, size: powerUpTexture.size())
        
        physicsBody?.categoryBitMask = GameScene.powerUpCategory
        physicsBody?.contactTestBitMask =  GameScene.playerCategory
        physicsBody?.collisionBitMask = GameScene.noCategory
    }
    
    func getPosition() -> CGPoint {
        return position
    }
    
    func doAction(pickedUpBy: [Player], otherPlayers: [Player]) {
        // override and call super
        self.pickedUpBy = pickedUpBy
        self.isActive = true
        self.otherPlayers = otherPlayers
        gameScene.despawnPowerUp(powerUp: self)
        Timer.scheduledTimer(timeInterval: upTime, target: self, selector: #selector(revertAction), userInfo: nil, repeats: false)
        animateActivePowerUp()
    }
    
    private func animateActivePowerUp() {
        if (affects == Affects.PICKED_UP_TEAM) {
            let pos = getActivePowerUpPosition(team1: gameScene.isPlayerInTeam1(player: pickedUpBy![0]))
            moveActivePowerUpToPosition(position: pos)
        } else if (affects == Affects.OTHER_TEAM) {
            let pos = getActivePowerUpPosition(team1: gameScene.isPlayerInTeam1(player: otherPlayers![0]))
            moveActivePowerUpToPosition(position: pos)
        } else if (affects == Affects.NONE) {
            // do nothing in this case
        }
    }
    
    private func getActivePowerUpPosition(team1: Bool) -> CGPoint {
        var pos: CGPoint
        if (team1) {
            pos = CGPoint(x: gameScene.score.scoreTeam1.position.x + 100, y: gameScene.score.scoreTeam1.position.y + 20)
        } else {
            pos = CGPoint(x: gameScene.score.scoreTeam2.position.x - 100, y: gameScene.score.scoreTeam2.position.y + 20)
        }
        return pos
    }
    
    private func moveActivePowerUpToPosition(position: CGPoint) {
        self.physicsBody?.categoryBitMask = GameScene.noCategory
        gameScene.addChild(self)
        self.run(SKAction.move(to: position, duration: 0.5))
        grayOut()
    }
    
    func grayOut() {
        let colorize = SKAction.colorize(with: .black, colorBlendFactor: 0.4, duration: 0.5)
        self.run(colorize)
        let smaller = SKAction.resize(toWidth: (self.texture?.size().width)! / 2, height: (self.texture?.size().height)! / 2, duration: 0.5)
        self.run(smaller)
    }
    
    @objc func revertAction() {
        // override and call super
        self.physicsBody?.categoryBitMask = GameScene.powerUpCategory
        self.isActive = false
        self.removeFromParent()
        highlight()
    }
    
    func highlight() {
        let colorize = SKAction.colorize(with: self.color, colorBlendFactor: 0, duration: 0.5)
        self.run(colorize)
        let bigger = SKAction.resize(toWidth: (self.texture?.size().width)!, height: (self.texture?.size().height)!, duration: 0.5)
        self.run(bigger)
    }
    
    public func setPosition(position: CGPoint) {
        self.position = position
    }
    
    public func getKey() -> String {
        return self.key
    }
}
