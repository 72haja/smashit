//
//  ResizePowerUp.swift
//  SmashIT
//
//  Created by Kai Widmaier on 02.06.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class ResizePowerUp: PowerUp {
    
    var pickedUpByPlayers: [Player] = []
    var oldSize: CGFloat!
    let modifierValue = 1.5
    
    init(scene: GameScene) {
        let textureName = "resizePowerUp"
        let upTime: TimeInterval = 8
        super.init(key: "Resize", powerUpTextureName: textureName, scene: scene, upTime: upTime, affects: Affects.PICKED_UP_TEAM)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func doAction(pickedUpBy: [Player], otherPlayers: [Player]) {
        super.doAction(pickedUpBy: pickedUpBy, otherPlayers: otherPlayers)
        pickedUpByPlayers = pickedUpBy
        oldSize = pickedUpBy[0].size.width / 2
        let newSize = oldSize * CGFloat(modifierValue)
        gameScene.field.setPhysicBodyForPlayer(players: pickedUpBy, radius: newSize)
    }
    
    override func revertAction() {
        super.revertAction()
        gameScene.field.setPhysicBodyForPlayer(players: pickedUpBy!, radius: oldSize)
    }
}
