//
//  SnailOnOtherTeam.swift
//  SmashIT
//
//  Created by student on 29.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class SnailOnOtherTeam: PowerUp {
    var players: [Player] = []
    var massData: CGFloat = 0
    
    init(scene: GameScene) {
        let textureName = "snailGreen"
        let upTime: TimeInterval =  12
        massData = 0.7
        super.init(key: "SnailOnOtherTeam", powerUpTextureName: textureName, scene: scene, upTime: upTime, affects: Affects.OTHER_TEAM)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func doAction(pickedUpBy: [Player], otherPlayers: [Player]) {
        super.doAction(pickedUpBy: pickedUpBy, otherPlayers: otherPlayers)
        players = otherPlayers
        for i in 0..<players.count{
            players[i].physicsBody?.mass = 2.5
        }
    }
    
    override func revertAction() {
        super.revertAction()
        for i in 0..<players.count {
            players[i].physicsBody?.mass = massData
        }
    }
}
