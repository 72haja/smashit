//
//  ZeitstrafePowerUp.swift
//  SmashIT
//
//  Created by Erik Wolf on 29.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation

class ZeitstrafePowerUp: PowerUp {
    var team1Complete : Bool = true
    var team2Complete : Bool = true

    init(scene: GameScene) {
        let textureName = "twoMinutes"
        let upTime: TimeInterval = 7
        super.init(key: "TwoMinutes", powerUpTextureName: textureName, scene: scene, upTime: upTime, affects: Affects.OTHER_TEAM)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func doAction(pickedUpBy: [Player], otherPlayers: [Player]) {
        super.doAction(pickedUpBy: pickedUpBy, otherPlayers: otherPlayers)
        otherPlayers.first?.removeFromParent()
    }
    
    override func revertAction() {
        super.revertAction()
        gameScene.addChild(otherPlayers!.first!)
    }
}
