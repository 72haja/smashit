//
//  SettingsViewController.swift
//  SmashIT
//
//  Created by Timo Kübler on 28.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    var gameConfig: GameConfig? = GameConfig()

    override func viewDidLoad() {
        super.viewDidLoad()
playButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleImageTap)))
        playButton.isUserInteractionEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let getIndex = playtypeSegmentOutlet.selectedSegmentIndex
        
        if getIndex == 0 {
            PointSegmentOutlet.isHidden = true
            TimeSegementOutlet.isHidden = false
        }
        if getIndex == 1 {
            PointSegmentOutlet.isHidden = false
            TimeSegementOutlet.isHidden = true
        }
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
//Outlets um die Segmented Controlls anzeigen zu lassen und Daten zu erhalten
    @IBOutlet weak var playtypeSegmentOutlet: UISegmentedControl!
    
    @IBOutlet weak var TimeSegementOutlet: UISegmentedControl!
    
    @IBOutlet weak var PointSegmentOutlet: UISegmentedControl!
    
    @IBOutlet weak var playButton: UIImageView!
    
    
    
    @IBAction func playTypeSegementisTapped(_ sender: UISegmentedControl)
    {
        let getIndex = playtypeSegmentOutlet.selectedSegmentIndex
        gameConfig?.gameMode = sender.titleForSegment(at: sender.selectedSegmentIndex)!
        if getIndex == 0 {
            PointSegmentOutlet.isHidden = true
            TimeSegementOutlet.isHidden = false
        }
        if getIndex == 1 {
            PointSegmentOutlet.isHidden = false
            TimeSegementOutlet.isHidden = true
        }
        
    }
    
    //Methode um Tap auf Image View des Playbutton zu erkennen
    @objc func handleImageTap() {
        let showMainView = self.storyboard?.instantiateViewController(withIdentifier: "MainStoryBoardID") as! GameViewController
        showMainView.gameConfig = gameConfig
        self.present(showMainView, animated: true, completion: nil)
        
        
    }
    
    // MARK: Actions
    @IBAction func playTypeMinuteSelection(_ sender: UISegmentedControl) {
        let endConditionAsString = sender.titleForSegment(at: sender.selectedSegmentIndex)!
        let intVal =  Int(endConditionAsString.prefix(upTo: endConditionAsString.index(after: endConditionAsString.startIndex)))
        gameConfig?.gameEndConditionTime = intVal!
    }
    
    @IBAction func playTypePointsSelection(_ sender: UISegmentedControl) {
        let endConditionAsString = sender.titleForSegment(at: sender.selectedSegmentIndex)!
        let intVal =  Int(endConditionAsString.prefix(upTo: endConditionAsString.index(after: endConditionAsString.startIndex)))
        gameConfig?.gameEndConditionPoints = intVal!
    }
    
    @IBAction func updateGameConfigField(_ sender: UISegmentedControl) {
        gameConfig?.gameField = sender.titleForSegment(at: sender.selectedSegmentIndex)!
    }
}
